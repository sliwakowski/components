//
//  ViewController.swift
//  Components
//
//  Created by Adam Śliwakowski on 23.04.2015.
//  Copyright (c) 2015 Sliwakowski. All rights reserved.
//

import UIKit
import Cartography
import SwiftyJSON

class ViewController: UIViewController {
    
    let defaults = NSUserDefaults.standardUserDefaults()
    var scrollView : UIScrollView?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var model : NSMutableDictionary = [
            "icon":"ios7-arrow-down",
            "title" : "Adam Śliwakowski ",
            "imageName" : "mybaze.jpg",
            "imageUrl" : "http://images.thecarconnection.com/sml/2015-bmw-i8_100439316_s.jpg"
        ]
        
        defaults.setObject(model, forKey: "model")
        scrollView = UIScrollView(frame: self.view.frame)
        scrollView!.backgroundColor = UIColor(rgba: "#dedfe4")
        self.view.addSubview(scrollView!)
        //createComponents()
        createRootComponent()
    }
    
    func createRootComponent() {
        
        var json = JSONTraverser.getJSON("mg")
        var actionPerformer = ActionPerformer(json: json)
        var traverser = JSONTraverser(json: json)
        var newLayout = traverser.traverseLayout()
        var cc : CompoundComponent = Component.parseComponent(newLayout) as! CompoundComponent
        scrollView!.addSubview(cc)
        var size = CGSizeMake(cc.getWidth(), cc.getHeight())
        scrollView!.contentSize = size
    }
    
    
    func clearScrollView() {
        for v in scrollView!.subviews {
            v.removeFromSuperview()
        }
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    func touchy(timer: NSTimer) {
        var data : NSDictionary = timer.userInfo as! NSDictionary
        println("Do something")
        UIAlertView(title: "Informacja", message: data["title"] as? String, delegate: self, cancelButtonTitle: "Anuluj").show()
        
        var model : NSDictionary = defaults.objectForKey("model") as! NSDictionary
        var mutableModel: NSMutableDictionary = model.mutableCopy() as! NSMutableDictionary
        mutableModel.setObject("- asdasd -", forKey: "title")
        
        defaults.setObject(mutableModel, forKey: "model")
        
    }
    
    func func1() {
        println("func1")
    }
    
    func notty(timer: NSTimer) {
        println("notty")
        var data : NSDictionary = timer.userInfo as! NSDictionary
        var json : JSON = Component.convertNSDictionaryToJSONObject(data)
        var jsonDict = json.dictionaryValue
        if let title = json["title"].string {
            UIAlertView(title: "Informacja", message: title, delegate: self, cancelButtonTitle: "Anuluj").show()
        }else {
            println(json["title"].error)
        }
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
}

