Pod::Spec.new do |s|

  s.name         = "Components"
  s.version      = "0.0.1"
  s.summary      = "Visual Components in Swift"
  s.platform      = :ios
  s.description  = <<-DESC
                   Create fully interactive layout with JSON file in Swift
                   DESC

  s.homepage     = "http://layouter.ovh"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "Adam Sliwakowski" => "sliwakowski@gmail.com" }

  s.ios.deployment_target = "8.0"

  s.source = { :git => "https://sliwakowski@bitbucket.org/sliwakowski/components.git", :tag => "#{s.version}" }
  s.source_files  = "Components/*.swift"

  s.dependency 'HanekeSwift'
  s.dependency 'Cartography'
  s.dependency 'Alamofire'
  s.dependency 'SwiftyJSON'

  s.requires_arc = true

end