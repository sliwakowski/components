//
//  ScrollViewComponent.swift
//  Components
//
//  Created by Adam Śliwakowski on 25.05.2015.
//  Copyright (c) 2015 Sliwakowski. All rights reserved.
//

import UIKit
import SwiftyJSON

class ScrollViewComponent: CompoundComponent, UIScrollViewDelegate {

    var scrollView : UIScrollView?
    var pageControl : UIPageControl?
    var scrollViewAttributes : ScrollViewComponentAttributes?
    
    convenience override init(json: JSON) {
        var attributes =  ScrollViewComponentAttributes(json: json)
        self.init(textViewAttributes: attributes)
        var components = Component.parseComponents(json)
        self.scrollViewAttributes = attributes
        self.orientation = .Horizontal
        addComponents(components)
        setupPageControl()
    }
    
    init(textViewAttributes: ScrollViewComponentAttributes) {
        super.init(attributes: textViewAttributes)
        scrollView = UIScrollView(frame: frame)
        attributes = textViewAttributes
        self.addSubview(scrollView!)
    }

    override func addComponents(components: [Component]) {
        for (index, childComponent) in enumerate(components) {
            childComponent.setupComponent(self.attributes.data)
            self.childComponents.append(childComponent)
            self.scrollView!.addSubview(childComponent)
        }
        injectDataToChildComponents()
        layoutComponents()
    }
    
    override func setupComponent(data: JSON) {
        super.setupComponent(data)
        self.scrollView!.pagingEnabled = true
        self.scrollView!.showsHorizontalScrollIndicator = false
        self.scrollView!.showsVerticalScrollIndicator = false
        self.scrollView!.delegate = self
        
        if let contentSize = self.scrollViewAttributes?.contentSize {
            self.scrollView!.contentSize = contentSize
        }
    }
    
    func setupPageControl() {
        self.pageControl = UIPageControl(frame: CGRectMake(0, self.attributes.size.height-20, self.attributes.size.width, 20))
        self.pageControl?.numberOfPages = count(self.childComponents);
        self.pageControl?.currentPageIndicatorTintColor = self.scrollViewAttributes?.currentIndicatorColor
        self.pageControl?.pageIndicatorTintColor = self.scrollViewAttributes?.indicatorColor
        self.addSubview(self.pageControl!)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        var pageWidth = self.attributes.size.width
        var pageAsFloat : Float = Float(self.scrollView!.contentOffset.x) / Float(pageWidth)
        var page = lroundf(pageAsFloat)
        self.pageControl?.currentPage = page
    }
    
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
