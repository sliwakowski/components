//
//  ListViewComponent.swift
//  Components
//
//  Created by Adam Śliwakowski on 24.05.2015.
//  Copyright (c) 2015 Sliwakowski. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class ListViewComponent: CompoundComponent {
   
    var listViewAttributes : ListViewComponentAttributes?
    
    override init(json: JSON) {
        var attributes =  ListViewComponentAttributes(json: json)
        super.init(attributes: attributes)
        self.listViewAttributes = attributes
        createList()
        //fetchData()
    }
    
    func createList() {
        clearComponent()
        var components : [Component] = []
        if let rowLayout = listViewAttributes!.rowLayout {
            var json = JSONTraverser.getJSON(rowLayout)
            var layout = json["layout"]
            var listData = listViewAttributes!.dataArray
            for index in 0..<count(listData) {
                var model = listData[index]
                var component = CompoundComponent(json: layout, data: model)
                component.attributes.data = model
                components.append(component)
            }
        }
        addComponents(components)
        refreshParentScrollViewContentSize()
    }
    
    func fetchData() {
        if let dataUrl = self.listViewAttributes?.dataUrl {
            Alamofire.request(.GET, dataUrl)
                .responseJSON { (req, res, json, error) in
                    var jsonResponse = JSON(json!)
                    self.listViewAttributes!.dataArray = jsonResponse.arrayValue
                    self.createList()
            }
        }
    }
    
    

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
