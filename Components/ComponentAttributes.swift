//
//  ComponentAttributes.swift
//  Components
//
//  Created by Adam Śliwakowski on 28.04.2015.
//  Copyright (c) 2015 Sliwakowski. All rights reserved.
//

import UIKit
import SwiftyJSON

enum SizeDimension : Int{
    case Width
    case Height
}

/// Attributes that can be applied to all components
public class ComponentAttributes : NSObject {
    
    /// Size of component as [width,height]. Examples: [300,200] or ["100%-50",30], where "100%-50" means 100% of screen width minus 50px
    public var size:               CGSize = CGSizeMake(0, 0)
    /// Insets (margin) as [top,left,bottom,right]
    public var insets:             UIEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
    /// Background color as "#RRGGBBAA" or "#RRGGBB"
    public var backgroundColor:    UIColor = UIColor.clearColor()
    /// Border color as "#RRGGBBAA" or "#RRGGBB"
    public var borderColor:        CGColor = UIColor.lightGrayColor().CGColor
    /// Border width as double value: 1.0
    public var borderWidth:        CGFloat = CGFloat(0)
    /// Corner radius as double value: 1.0
    public var cornerRadius:       CGFloat = 0
     /// Data provided as array of JSON documents. Example: [ {"a":"b"},{"c":"d"} ]
    public var data : JSON = JSON([:])
    var json : JSON = JSON([:])
    ///  Method that should be fired when tapped on component. Should be declared in ViewController
    public var selector: String?
    
    override init() {
        
    }
    
    init(size: CGSize) {
        self.size = size
    }
    
    init(size: CGSize, insets: UIEdgeInsets) {
        self.size = size
        self.insets = insets
    }
    /**
    Initializes a new ComponentAttributes with values provided in JSON format
    
    :param: object containing all variables
    
    :returns: object with parsed attributes from JSON object
    */
    public init(json: JSON){
        super.init()
        var attributes = json["attributes"];
        var size = attributes["size"]
        var insets = attributes["insets"]
        self.json = attributes
        self.size = createSize(size)
        self.insets = UIEdgeInsetsMake(
            CGFloat(insets[0].doubleValue),
            CGFloat(insets[1].doubleValue),
            CGFloat(insets[2].doubleValue),
            CGFloat(insets[3].doubleValue)
        )
        if let backgroundColor = attributes["backgroundColor"].string where backgroundColor.hasPrefix("#"){
            self.backgroundColor = UIColor(rgba: backgroundColor)
        }
        if let borderColor = attributes["borderColor"].string where borderColor.hasPrefix("#"){
            self.borderColor = UIColor(rgba: borderColor).CGColor

        }
        if let data = json["data"].dictionary {
            self.data = json["data"]
        }
        if let selector = attributes["selector"].string {
            self.selector = selector
        }
        
        self.borderWidth = CGFloat(attributes["borderWidth"].doubleValue)
        self.cornerRadius = CGFloat(attributes["cornerRadius"].doubleValue)
    }
    
    func createSize(json: JSON)->CGSize {
        var size = CGSizeMake(0, 0)
        // ["100%-30"]
        if let widthAsString = json[0].string {
            size.width = calculateMetricsFromString(widthAsString, sizeDimension: .Width)
        } else {
            size.width = CGFloat(json[0].doubleValue)
        }
        if let heightAsString = json[1].string {
            size.height = calculateMetricsFromString(heightAsString, sizeDimension: .Height)
        } else {
            size.height = CGFloat(json[1].doubleValue)
        }
        
        return size
    }
    func calculateMetricsFromString(string: String, sizeDimension: SizeDimension)->CGFloat {
        var computedSize = calculatePercentMetrics(string, sizeDimension: sizeDimension)
        computedSize = applyAdditionalMetrics(computedSize, string: string)
        return CGFloat(computedSize)
    }
    
    func calculatePercentMetrics(string: String, sizeDimension: SizeDimension)->Double {
        var sizeInPercentsAsString = string.componentsSeparatedByString("%")[0]
        var sizeInPercents = (sizeInPercentsAsString as NSString).doubleValue
        var computedSize = 0.0
        if sizeDimension == .Width {
            computedSize = sizeInPercents / 100 * Double(screenWidth)
        } else if sizeDimension == .Height {
            computedSize = sizeInPercents / 100 * Double(screenHeight)
        }
        return computedSize
    }
    
    func applyAdditionalMetrics(computedSize: Double, string: String)-> Double {
        var computedSize = computedSize
        var additionalMetrics = string.componentsSeparatedByString("%")[1]
        if count(additionalMetrics)>0 {
            var additionalMetricsValueAsString = additionalMetrics.substringFromIndex(advance(string.startIndex, 1))
            var additionalMetricsValue = (additionalMetricsValueAsString as NSString).doubleValue
            if additionalMetrics.hasPrefix("-") {
                computedSize = computedSize - additionalMetricsValue
            } else if additionalMetrics.hasPrefix("+") {
                computedSize = computedSize + additionalMetricsValue
            }

        }
        return computedSize
    }
}

/// Subclass of ComponentAttributes. Attributes that can be applied to TextViewComponent objects
public class TextViewComponentAttributes : ComponentAttributes {
    
    ///  Text of the component
    public var text: String = ""
    /// Text color as "#RRGGBBAA" or "#RRGGBB"
    public var textColor: UIColor = UIColor.blackColor()
     /// Font provided as JSON dictionary with style and size attributes. Example: {"style":"bold", "size":13.0}
    public var font: UIFont = UIFont.systemFontOfSize(13.0)
    
    override init() {
        super.init()
    }
    
    override init(json: JSON){
        super.init(json: json)
        var attributes = json["attributes"];
        self.text = attributes["text"].stringValue
        if let textColor = attributes["textColor"].string {
            self.textColor = UIColor(rgba: textColor)
        }
        if let fontSize = attributes["font"]["size"].double, let fontStyle = attributes["font"]["style"].string {
            if(fontStyle=="bold"){
               self.font = UIFont.boldSystemFontOfSize(CGFloat(fontSize))
            } else {
                self.font = UIFont.systemFontOfSize(CGFloat(fontSize))
            }
            
        }
    }
}
/// Subclass of ComponentAttributes. Attributes that can be applied to ImageViewComponent objects
public class ImageViewComponentAttributes : ComponentAttributes {
    
    /// Image name of image from project. Example: "photo.jpg"
    public var imageName: String?
    /// Url of the remote hosted image. Example: "http://google.com/myphoto.png"
    public var imageUrl: String?
     /// Content mode of ImageView. AspectFit | AspectFill
    /**
    Content mode of ImageView.
    
    - AspectFit: Keeps aspect ratio and fit the image inside frame.
    - AspectFill: Keeps aspect ratio and fill the image frame.
    */
    public var contentMode: String = "AspectFit"
    override init() {
        super.init()
    }
    
    override init(json: JSON){
        super.init(json: json)
        var attributes = json["attributes"];
        if let imageName = attributes["imageName"].string {
            self.imageName = imageName
        }
        if let imageUrl = attributes["imageUrl"].string {
            self.imageUrl = imageUrl
        }
        if let contentMode = attributes["contentMode"].string {
            self.contentMode = contentMode
        }
    }
}

/// Subclass of ComponentAttributes. Attributes that can be applied to ButtonComponent objects
public class ButtonComponentAttributes : ComponentAttributes {
    
    // TODO
    /// Image name of image from project. Example: "photo.jpg"
    public var imageName: String = ""
    // TODO
    /// Background color as "#RRGGBBAA" or "#RRGGBB"
    public var tintColor: UIColor = UIColor.blackColor()
    
    override init() {
        super.init()
    }
    
    override init(json: JSON){
        super.init(json: json)
        var attributes = json["attributes"];
        if let imageName = attributes["imageName"].string {
            self.imageName = imageName
        }
        if let tintColor = attributes["tintColor"].string {
            self.tintColor = UIColor(rgba: tintColor)
        }
    }
}

/// Subclass of ComponentAttributes. Attributes that can be applied to ScrollViewComponent objects
public class ScrollViewComponentAttributes : ComponentAttributes {
    
    /**
    Orientation of components inside ScrollViewComponent
    
    - Horizontal: From left to right
    - Vertical: (Default) From top to bottom
    */
    public var orientation : ComponentOrientation = .Horizontal
    /// Content size of component as [width,height]. Examples: [300,200] or ["100%-50",30], where "100%-50" means 100% of screen width minus 50px
    public var contentSize : CGSize?
    /// Background color of the coloured dot in active state at the bottom of ScrollViewComponent
    public var currentIndicatorColor : UIColor = UIColor(rgba: "#FF0000")
    /// Background color of the coloured dot in inactive state at the bottom of ScrollViewComponent
    public var indicatorColor : UIColor = UIColor(rgba: "#FFFFFFCC")
    
    override init() {
        super.init()
    }
    
    override init(json: JSON){
        super.init(json: json)
        var attributes = json["attributes"];
        var contentSize = attributes["contentSize"]
        var pageControl = attributes["pageControl"]
        
        if let orientation = attributes["orientation"].string where orientation == "Vertical"{
            self.orientation = .Vertical
        }
        if let currentIndicatorColor = pageControl["currentIndicatorColor"].string {
            self.currentIndicatorColor = UIColor(rgba: currentIndicatorColor)
        }
        if let indicatorColor = pageControl["indicatorColor"].string {
            self.indicatorColor = UIColor(rgba: indicatorColor)
        }
        self.contentSize = createSize(contentSize)
    }
}

class ListViewComponentAttributes : ComponentAttributes {
    var dataArray: [JSON] = []
    var dataUrl: String?
    var rowLayout: String?
    
    override init() {
        super.init()
    }
    
    override init(json: JSON){
        super.init(json: json)
        var attributes = json["attributes"];
        if let data = attributes["dataArray"].array {
            self.dataArray = data
        }
        if let dataUrl = attributes["dataUrl"].string {
            self.dataUrl = dataUrl
        }
        self.rowLayout = attributes["rowLayout"].stringValue
    }
}