//
//  ActionPerformer.swift
//  Components
//
//  Created by Adam Śliwakowski on 24.05.2015.
//  Copyright (c) 2015 Sliwakowski. All rights reserved.
//

import UIKit
import SwiftyJSON

class ActionPerformer: NSObject {

    var variables : JSON = JSON("")
    var functions : JSON = JSON("")
    
    init(json: JSON){
        super.init()
        self.variables = json["variables"]
        self.functions = json["functions"]
        
        perform("+init")
    }
    
    func perform(functions: [JSON]) {
        for function in functions {
            perform(function.stringValue)
        }
    }
    
    
    func perform(functionName: String){
        var function = self.functions[functionName]
        var returnKey = function["return"].stringValue
        var parameter = function["parameter"].arrayValue
        var action = function["action"].stringValue
        
        switch action {
        case "addNumbers" :
            variables[returnKey] = JSON(Action.addNumbers(parameter))
        case "perform" :
            perform(parameter)
        default :
            println()
        }
    }
}
