//
//  Action.swift
//  Components
//
//  Created by Adam Śliwakowski on 24.05.2015.
//  Copyright (c) 2015 Sliwakowski. All rights reserved.
//

import UIKit
import SwiftyJSON

class Action: NSObject {
    
    class func addNumbers(numbers: [JSON]) -> Double {
        var result = 0.0
        for number in numbers {
            result+=number.doubleValue
        }
        return result
    }
    
}
