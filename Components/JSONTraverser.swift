//
//  JSONTraverser.swift
//  Components
//
//  Created by Adam Śliwakowski on 23.05.2015.
//  Copyright (c) 2015 Sliwakowski. All rights reserved.
//

import Foundation
import SwiftyJSON

public class JSONTraverser : NSObject {
    
    var variables : JSON = JSON(0)
    var layout : JSON = JSON(0)
    
    public init(json: JSON){
        super.init()
        self.variables = json["variables"]
        self.layout = json["layout"]
        includeExternalFiles()
    }
    
    func includeExternalFiles() {
        var variablesToInclude = self.variables["include"].arrayValue
        for externalFile in variablesToInclude {
            var externalFileContent = JSONTraverser.getJSON(externalFile.stringValue)
            for externalFileKey in externalFileContent.dictionaryValue.keys.array {
                self.variables[externalFileKey] = externalFileContent[externalFileKey]
            }
        }
    }
    
    func replaceVariables() {
        traverseLayout()
    }
    
    public func traverseLayout() -> JSON{
        return traverseJSON(layout)
    }
    
    func traverseJSON(json: JSON) -> JSON{
        if let jsonDictionary = json.dictionary {
            return traverseDictionary(json)
        } else if let jsonArray = json.array{
            return traverseArray(json)
        }
        return json
    }
    
    func traverseArray(array: JSON) -> JSON{
        var jsonArray = array
        for index in 0...count(jsonArray.arrayValue) {
            if let dictionary = jsonArray[index].dictionary {
                var json = jsonArray[index]
                jsonArray[index] = traverseDictionary(json)
            }
        }
        return jsonArray
    }
    
    func traverseDictionary(dictionary: JSON) -> JSON{
        var jsonDictionary = dictionary
        var keys = jsonDictionary.dictionaryValue.keys.array
        var sortedKeys = sorted(keys, >)
        for key in sortedKeys {
            if let jsonValueString = jsonDictionary[key].string{
                if jsonValueString.hasPrefix("_component_") {
                    jsonDictionary = assignComponent(jsonValueString, json: jsonDictionary)
                    jsonDictionary = traverseDictionary(jsonDictionary)
                }
                else if jsonValueString.hasPrefix("_"), let newValue = variables.dictionaryValue[jsonValueString] {
                    jsonDictionary[key] = newValue
                    jsonDictionary = traverseDictionary(jsonDictionary)
                }
            } else if key.hasPrefix("_ext_") {
                jsonDictionary = extendValuesForKey(key, json: jsonDictionary)
            } else {
                let jsonValue = jsonDictionary[key]
                jsonDictionary[key] = traverseJSON(jsonValue)
            }
        }
        return jsonDictionary
    }
    
    func assignComponent(componentName: String, json: JSON) -> JSON {
        var jsonDictionary = json
        var fetchedComponent = variables[componentName]
        var keys = fetchedComponent.dictionaryValue.keys.array
        for key in keys {
            jsonDictionary[key] = fetchedComponent[key]
        }
        
        return jsonDictionary
    }
    
    func extendValuesForKey(key: String, json: JSON) -> JSON {
        var jsonDictionary = json
        let attributeToExtend = key.substringFromIndex(advance(key.startIndex, count("_ext_")))
        let extensionDictionary = jsonDictionary[key].dictionary
        if let subkeys = extensionDictionary?.keys.array {
            for subkey in subkeys {
                jsonDictionary[attributeToExtend][subkey] = jsonDictionary[key][subkey]
            }
        }
        
        jsonDictionary[key] = JSON(0)
        return jsonDictionary
    }
    
    class func getJSON(file: String) -> JSON{
        if let path = NSBundle.mainBundle().pathForResource(file, ofType: "json") {
            if let jsonData: NSData = NSData(contentsOfURL: NSURL(fileURLWithPath: path)!) {
                let json = JSON(data: jsonData)
                return json
            }
        }
        return JSON("")
    }
    
}