//
//  ImageViewComponent.swift
//  Components
//
//  Created by Adam Śliwakowski on 01.05.2015.
//  Copyright (c) 2015 Sliwakowski. All rights reserved.
//

import UIKit
import Haneke
import SwiftyJSON

class ImageViewComponent: Component {

    var imageView : UIImageView?
    var imageViewAttributes : ImageViewComponentAttributes?
    
    convenience init(json: SwiftyJSON.JSON){
        var attributes =  ImageViewComponentAttributes(json: json)
        self.init(imageViewAttributes: attributes)
    }
    
    init(imageViewAttributes: ImageViewComponentAttributes){
        super.init(attributes: imageViewAttributes)
        self.imageViewAttributes = imageViewAttributes
        self.layer.masksToBounds = true
        addImageViewToComponent()
    }
    
    func addImageViewToComponent() {
        imageView = UIImageView(frame: frame)
        imageView?.layer.cornerRadius = imageViewAttributes!.cornerRadius
        imageView?.layer.masksToBounds = true
        imageView?.contentMode = UIViewContentMode.ScaleAspectFill
        self.addSubview(imageView!)
        setContentMode()
    }
    
    func setContentMode() {
        var contentMode = self.imageViewAttributes!.contentMode
        switch contentMode {
        case "AspectFill" :
            imageView?.contentMode = UIViewContentMode.ScaleAspectFill
        case "AspectFit" :
            imageView?.contentMode = UIViewContentMode.ScaleAspectFit
        default :
            imageView?.contentMode = UIViewContentMode.ScaleAspectFit
        }
    }
    
    override func setupComponent(data: SwiftyJSON.JSON) {
        super.setupComponent(data)
        setImage(data)
    }
    
    private func setImage(data: SwiftyJSON.JSON) {
        if
            let imageNameModel = imageViewAttributes?.imageName,
            let model : String = getModelFromValue(imageNameModel),
            let imageName = data[model].string,
            let image = UIImage(named: imageName)
        {
            self.imageView?.image = image
        }
        else if
            let imageUrlModel = imageViewAttributes?.imageUrl,
            let model : String = getModelFromValue(imageUrlModel),
            let imageUrl = data[model].string,
            let imageURL : NSURL = NSURL(string: imageUrl),
            let imageView : UIImageView = self.imageView
        {
            imageView.hnk_setImageFromURL(imageURL, placeholder: UIImage(named: "mybaze.jpg"), format: nil, failure: nil, success: nil)
        }
        else if
            let imageName = imageViewAttributes?.imageName,
            let imageView : UIImageView = self.imageView,
            let image = UIImage(named: imageName)
        {
            imageView.image = image
        }
        else if
            let imageUrl = imageViewAttributes?.imageUrl,
            let imageURL : NSURL = NSURL(string: imageUrl),
            let imageView : UIImageView = self.imageView
        {
            
           imageView.hnk_setImageFromURL(imageURL, placeholder: UIImage(named: "mybaze.jpg"), format: nil, failure: nil, success: nil)
        }
    }
    

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
