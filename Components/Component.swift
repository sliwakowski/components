//
//  Component.swift
//  Components
//
//  Created by Adam Śliwakowski on 23.04.2015.
//  Copyright (c) 2015 Sliwakowski. All rights reserved.
//

import UIKit
import Cartography
import SwiftyJSON

public enum ComponentOrientation {
    case Vertical
    case Horizontal
}

public class Component: UIView {
    
    var attributes : ComponentAttributes = ComponentAttributes()
    
    init(attributes: ComponentAttributes) {
        super.init(frame:CGRectMake(0, 0, attributes.size.width, attributes.size.height))
        self.attributes = attributes
    }
    
    public class func loadLayoutFromFileIntoView(file: String, scrollView: UIScrollView){
        var json = JSONTraverser.getJSON(file)
        Component.loadJSONLayoutIntoView(json, scrollView: scrollView)
    }
    
    public class func loadJSONLayoutIntoView(json: JSON, scrollView: UIScrollView){
        for subview in scrollView.subviews{
            subview.removeFromSuperview()
        }
        var traverser = JSONTraverser(json: json)
        var newLayout = traverser.traverseLayout()
        var cc : CompoundComponent = Component.parseComponent(newLayout) as! CompoundComponent
        scrollView.addSubview(cc)
        var size = CGSizeMake(cc.getWidth(), cc.getHeight())
        scrollView.contentSize = size
    }
    
    func setupComponent(data: JSON) {
        self.backgroundColor = attributes.backgroundColor
        self.layer.borderWidth = attributes.borderWidth
        self.layer.borderColor = attributes.borderColor
        self.layer.cornerRadius = attributes.cornerRadius

        if let selector = self.attributes.selector where !(self is ButtonComponent) {
            self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "buttonAction:"))
        }
        
        attributes.data = data
        setupComponentFromData()
    }
    
    func setupComponentFromData() {
        if let backgroundColorValue = self.attributes.json["backgroundColor"].string
            where backgroundColorValue.hasPrefix("{") {
                if let model = getModelFromValue(backgroundColorValue) {
                    let backgroundColor = self.attributes.data[model].stringValue
                    self.backgroundColor = UIColor(rgba: backgroundColor)
                }
        }
    }
    
    func parentController() -> UIViewController {
        var object : AnyObject = self.nextResponder()!
        while( !(object.isKindOfClass(UIViewController)) ) {
            object = object.nextResponder()!
        }
        return object as! UIViewController;
    }
    
    func buttonAction(sender: Component) {
        println("action");
        var controller = parentController()
        var jsonDict = self.attributes.data.dictionaryValue
        let userInfo = Component.convertJSONObjectToNSDictionary(self.attributes.data)
        
        if controller.respondsToSelector(Selector(self.attributes.selector!)) {
            var timer = NSTimer.scheduledTimerWithTimeInterval(0, target: controller, selector: Selector(self.attributes.selector!), userInfo: userInfo, repeats: false)
        } else {
            println("Layouter: unhandled method")
        }
        
    }
    
    public class func convertJSONObjectToNSDictionary(json: JSON)->NSDictionary{
        if let jsonString = json.rawString(encoding: NSUTF8StringEncoding, options: NSJSONWritingOptions.allZeros) {
            var nsdictionary = NSDictionary(dictionary: ["json":jsonString])
            return nsdictionary
        }
        return NSDictionary(dictionary: ["json":""])
    }
    
    public class func convertNSDictionaryToJSONObject(nsdictionary: NSDictionary)->JSON{
        if let jsonString : NSString = nsdictionary.objectForKey("json") as? NSString{
            let json = JSON(data:jsonString.dataUsingEncoding(NSUTF8StringEncoding)!)
            return json
        }
        return JSON("")
    }
    
    public class func parseComponent(json: JSON) -> Component {
        var type = json["type"].stringValue
        switch type {
        case "CompoundComponent" :
            return CompoundComponent(json: json)
        case "TextViewComponent" :
            return TextViewComponent(json: json)
        case "ImageViewComponent" :
            return ImageViewComponent(json: json)
        case "ButtonComponent" :
            return ButtonComponent(json: json)
        case "ListViewComponent" :
            return ListViewComponent(json: json)
        case "ScrollViewComponent" :
            return ScrollViewComponent(json: json)
        default :
            return Component()
        }
        
    }
    
    public class func parseComponents(json: JSON) -> [Component]{
        var components : [Component] = []
        var childComponents = json["components"].arrayValue
        for childComponent in childComponents {
            components.append(Component.parseComponent(childComponent))
        }
        return components
    }
    
    
    
    
    
    
    
    
    
    
    func getModelFromValue(value: String) -> String? {
        if (value.hasPrefix("{") && value.hasSuffix("}")) {
            var model = value
            model.removeAtIndex(model.endIndex.predecessor())
            model.removeAtIndex(model.startIndex)
            return model
        }
        return nil
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
    }
    required public init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
