//
//  TextViewComponent.swift
//  Components
//
//  Created by Adam Śliwakowski on 28.04.2015.
//  Copyright (c) 2015 Sliwakowski. All rights reserved.
//

import UIKit
import SwiftyJSON

class TextViewComponent: Component {
    
    var textView : UITextView?
    
    convenience init(json: JSON) {
        var attributes =  TextViewComponentAttributes(json: json)
        self.init(textViewAttributes: attributes)
    }
    
    init(textViewAttributes: TextViewComponentAttributes) {
        super.init(attributes: textViewAttributes)
        textView = UITextView(frame: frame)
        attributes = textViewAttributes
        self.addSubview(textView!)
    }
    
    override func setupComponent(data: JSON) {
        super.setupComponent(data)
        var textViewAttributes : TextViewComponentAttributes = self.attributes as! TextViewComponentAttributes
        
        textView?.backgroundColor = UIColor.clearColor()
        textView?.editable = false
        textView?.scrollEnabled = false
        textView?.selectable = false
        textView?.contentInset = UIEdgeInsets(top: -8,left: -5,bottom: 0,right: 0)
        textView?.font = textViewAttributes.font
        textView?.textColor = textViewAttributes.textColor
        setText(textViewAttributes,data: data)
        setupComponentFromData()
    }
    override func setupComponentFromData() {
        if let textColorValue = self.attributes.json["textColor"].string
            where textColorValue.hasPrefix("{") {
                if let model = getModelFromValue(textColorValue) {
                    let textColor = self.attributes.data[model].stringValue
                    self.textView?.textColor = UIColor(rgba: textColor)
                }
        }
    }
    
    func setText(textViewAttributes: TextViewComponentAttributes, data: JSON) {
        if let model : String = getModelFromValue(textViewAttributes.text), let value : String = data[model].string {
            textView?.text = value
        } else {
            textView?.text = textViewAttributes.text
        }
    }
    
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
