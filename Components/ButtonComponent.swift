//
//  ButtonComponent.swift
//  Components
//
//  Created by Adam Śliwakowski on 03.05.2015.
//  Copyright (c) 2015 Sliwakowski. All rights reserved.
//

import UIKit
import SwiftyJSON

class ButtonComponent: Component {
    
    var button : UIButton?
    var buttonAttributes : ButtonComponentAttributes?
    
    convenience init(json: JSON){
        var buttonAttributes =  ButtonComponentAttributes(json: json)
        self.init(buttonAttributes: buttonAttributes)
    }
    
    init(buttonAttributes: ButtonComponentAttributes) {
        super.init(attributes: buttonAttributes)
        self.attributes = buttonAttributes
        self.buttonAttributes = buttonAttributes
        addButtonToComponent()
    }
    
    func addButtonToComponent() {
        button = UIButton.buttonWithType(UIButtonType.Custom) as? UIButton
        button?.frame = self.frame
        self.addSubview(button!)
    }
    
    override func setupComponent(data: JSON) {
        super.setupComponent(data)
        button?.tintColor = buttonAttributes?.tintColor
        button!.addTarget(self, action: NSSelectorFromString("buttonAction:"), forControlEvents: UIControlEvents.TouchUpInside)
        setButtonImage(data)
    }
    
    func setButtonImage(data: JSON) {
        if
            let model : String = getModelFromValue(buttonAttributes!.imageName),
            let imageName = data[model].string {
            button?.setImage(
                UIImage(named: imageName)?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate),
                forState: UIControlState.Normal)
        } else {
            button?.setImage(
                UIImage(named: buttonAttributes!.imageName)?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate),
                forState: UIControlState.Normal)
        }
    }
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
