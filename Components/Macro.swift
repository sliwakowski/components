//
//  Macro.swift
//  Components
//
//  Created by Adam Śliwakowski on 28.05.2015.
//  Copyright (c) 2015 Sliwakowski. All rights reserved.
//

import Foundation
import UIKit

let screenBounds = UIScreen.mainScreen().bounds
let screenSize   = screenBounds.size
let screenWidth  = screenSize.width
let screenHeight = screenSize.height
var IDIOM = UIDevice.currentDevice().userInterfaceIdiom