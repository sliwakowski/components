//
//  ScrollComponent.swift
//  Components
//
//  Created by Adam Śliwakowski on 26.04.2015.
//  Copyright (c) 2015 Sliwakowski. All rights reserved.
//

import UIKit
import Cartography

class ScrollComponent: CompoundComponent {

    private var scrollView : UIScrollView = UIScrollView(frame: CGRectMake(0, 0, 0, 0))
    
    override init(components: [Component], orientation: ComponentOrientation) {
        super.init(components: components, orientation: orientation)
        
    }
    
    override func addSubview(view: UIView) {
        super.addSubview(view)
        scrollView.addSubview(view)
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
